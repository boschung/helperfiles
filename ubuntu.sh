# setup ubuntu with a few tools
sudo apt install libavcodec-extra
sudo apt-get install vlc
sudo apt-get install flashplugin-installer
sudo apt install openjdk-11-jdk openjdk-11-jre
sudo apt-get install p7zip p7zip-full p7zip-rar 
sudo apt-get -f install steam 

sudo apt install putty
sudo apt-get install filezilla
sudo apt install virtualbox
sudo apt-get install wine # .exe files "wine NameExampleLTSPICE"
sudo apt-get install docker-ce # sudo docker run hello-world
sudo apt-get install eclipse
sudo apt-get install notepadqq
sudo apt-get install gparted
sudo apt-get install git

# Golang
sudo apt install golang-go #-->create file .go into a folder named "h"
# go build #start the app: ./h

#Notes and lifehacks
# pycharm is to finde directly in the Ubuntu SW Center
# ssh pi@192.168.0.101 -X  // gedit anyText.txt
# i ="when installing use -y command to say automatically yes;
# chmod +x dateiname.sh //use that to make .sh executable
# sudo dpkg --install synology-cloud-station-drive-4204.x86_64.deb