# basic useage example of pynput
# Tanks to https://nitratine.net/blog/post/simulate-mouse-events-in-python/
# Python 3.7.3
# pynput 1.4.2

import time
from pynput.mouse import Button, Controller

RC=1
LC=2

def doMouseInstructionAbsolute(x, y, click, brake):
    mouse.position = (x, y)
    if(click==RC):
        mouse.click(Button.right, 1)
    if (click == LC):
        mouse.click(Button.left, 1)
    time.sleep(brake)


def getMousePosition():
    print ("Current position: " + str(mouse.position))


mouse = Controller()
doMouseInstructionAbsolute(-1500, 378, RC, 1) #Activate Window
doMouseInstructionAbsolute(-1358, 830, LC, 1) #New
doMouseInstructionAbsolute(-1029, 389, LC, 1) #Folder

#to setup the instructions
while 1:
    getMousePosition()
    time.sleep(0.5)

    
    

'''
Possible extensions:
# Click the middle button
mouse.click(Button.middle, 1)
# Click the left button ten times
mouse.click(Button.left, 10)
mouse.press(Button.left)
mouse.release(Button.left)
# Scroll up two steps
mouse.scroll(0, 2)
# Scroll right five steps
mouse.scroll(5, 0)
#further: keyboard = KeyboardController()
#from pynput.keyboard import Key, Controller as KeyboardController
#from pynput.mouse import Button, Controller as MouseController
'''
