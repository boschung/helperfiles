REM Call this file with admin rights CMD

shell = "powershell"

REM Installation von chocolatey
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

REM Installation of differet tools

choco install -y teamviewer

choco install -y cloudstation
choco install -y 7zip.install

choco install -y notepadplusplus
choco install -y haroopad

choco install -y googlechrome
choco install -y firefox  
choco install -y opera

choco install -y flashplayerplugin 
choco install -y vlc 

choco install -y teraterm --version=4.102
choco install -y putty
choco install -y putty.install 
choco install -y jre8
choco install -y pycharm-community 
choco install -y winmerge
choco install -y hyper --version 1.3.3
choco install -y tortoisesvn
choco install -y git.install 
choco install -y tortoisegit

choco install -y gimp 
choco install -y libreoffice 
choco install -y virtualbox 
choco install -y adobereader
choco install -y pdfcreator
choco install -y simple-sticky-notes

REM Installation von Python (add a few pip commands if needed)
choco install -y anaconda3
choco install -y pip
REM setx path "%PATH%;C:\tools\Anaconda3" /M
REM pip install anyLib==0.99.921

REM choco install -y docker-for-windows REM Win10 needs restart to modifie Virtualisation
REM choco install -y docker  test: docker run hello-world

REM choco install -y steam
REM TODO Cloude Station synology
REM TODO: Eclipse install with packages (preconfiguration?)

REM TODO: sourcen kopieren
REM copy s\* C:\myIDE

