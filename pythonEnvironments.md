# Simple guide for virtual environemtnts in Python

## Install Precondition:
- PATH has to hold the required python installation (ex,: 32bit 3.7.3)

``` pip install virtualenv ```

## Setup an environment
```C:\pythonVenv>virtualenv myEnv  ```

alternatively with a specific version of Python  

``` C:\pythonVenv>virtualenv myCondaEnv -p C:\Anaconda3\python.exe ```
or
```virtualenv --python=PYTHONPATH/python3.7.1/python.exe Destination_my_ENV```
  
## Use the created environment
``` \myEnv\Scripts\activate ```

```deactivate```

or use the path C:/FullPath/Scripts/python.exe myScript.py

## Extend the environent with further packages
Ex.: pip install Flask==1.0.1

## Export an environment
pip freeze > requirements.txt

## Import an environment
pip install -r path/to/requirements.txt
